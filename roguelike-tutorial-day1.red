Red [needs 'View author "Jaume Green"]
;Inspiration from http://www.mycode4fun.co.uk/example-scripts

font-size: 12
view/options  [ title " Roguelike " backdrop white size 500x500
at 250x250 player: text "@"
] [
    actors: context [
       on-key: func [face event][
            switch event/key [
                up [ player/offset/y: player/offset/y - font-size ]
                down [ player/offset/y: player/offset/y + font-size ]
                left [player/offset/x: player/offset/x - font-size]
                right [player/offset/x: player/offset/x + font-size]
            ]
        ]
    ]
]